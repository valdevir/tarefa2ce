package ce;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * Classe respons�vel pela interface principal.
 * 
 * @author valdevir
 *
 */
public class Interface extends JFrame {
	private static final String AUTO_ADAPTATIVA = "Auto Adaptativa";

	private static final String FEEDBACK_DA_BUSCA = "Feedback da busca";

	private static final String DETERMINISTICO = "Determin�stico";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GraficoConvergencia graficoConvergencia;

	private JPanel painelPrincipal;
	private JPanel painelLeft;
	private JPanel painelRight;
	private javax.swing.JPanel painelGrafico;
	private final javax.swing.JTextArea logArea;
	private JScrollPane jScrollPane1;
	private JPanel painelDados;
	private JLabel labelCrossver;
	private JLabel labelPopulacao;
	private JLabel labelGeracoes;
	private JLabel labelTipoMutacao;
	private JTextField valorCrossover;
	private JTextField valoPopulacao;
	private JTextField valoGeracoes;
	private JComboBox<String> comboTipoMutacao;
	private JButton botaoIniciar;

	// private jLabel4 = new javax.swing.JLabel();
	public Interface() {
		super("Computa��o Evolutiva - Tarefa 1 ");
		final Tarefa2 tarefa = new Tarefa2();
		// tarefa.setaParametrosProblema();
		Algoritimo algoritimo = new Algoritimo();
		setPreferredSize(new Dimension(800, 600));
		painelPrincipal = new JPanel();
		painelLeft = new JPanel(new BorderLayout());
		painelDados = new JPanel();
		painelDados.setLayout(new GridLayout(4, 2));
		labelCrossver = new JLabel("Crossover: ");
		labelPopulacao = new JLabel("Popula��o: ");
		labelGeracoes = new JLabel("N�mero de Gera��es: ");
		labelPopulacao = new JLabel("Popula��o: ");
		labelTipoMutacao = new JLabel("Tipo Muta��o: ");
		valorCrossover = new JTextField();
		valorCrossover.setText(new Double(algoritimo
				.getTaxaDeCrossoverDefault()).toString());
	
		valoPopulacao = new JTextField();
		valoPopulacao.setText(new Integer(algoritimo.getTamPopDefault())
				.toString());
		valoGeracoes = new JTextField();
		valoGeracoes.setText(new Integer(algoritimo.getNumMaxGeracoesDefault())
				.toString());

		comboTipoMutacao = new JComboBox<String>();
		comboTipoMutacao.addItem(DETERMINISTICO);
		comboTipoMutacao.addItem(FEEDBACK_DA_BUSCA);
		comboTipoMutacao.addItem(AUTO_ADAPTATIVA);
		// Constroi painel de dados.
		painelDados.add(labelCrossver);
		painelDados.add(valorCrossover);
		painelDados.add(labelPopulacao);
		painelDados.add(valoPopulacao);
		painelDados.add(labelGeracoes);
		painelDados.add(valoGeracoes);
		painelDados.add(labelTipoMutacao);
		painelDados.add(comboTipoMutacao);
		painelDados.setBorder(BorderFactory.createLoweredBevelBorder());
		painelLeft.setBorder(BorderFactory.createLoweredBevelBorder());
		painelLeft.add(painelDados, BorderLayout.NORTH);
		logArea = new JTextArea();
		logArea.setEditable(false);
		logArea.setColumns(20);
		logArea.setRows(26);
		jScrollPane1 = new JScrollPane(logArea);
		// jScrollPane1.setViewportView(logArea);
		painelLeft.add(jScrollPane1, BorderLayout.SOUTH);
		painelGrafico = new JPanel();
		painelRight = new JPanel();
		painelRight.add(painelGrafico);
		painelPrincipal.setLayout(new BorderLayout());
		painelPrincipal.add(painelLeft, BorderLayout.WEST);
		painelPrincipal.add(painelRight, BorderLayout.EAST);
		montaGraficoConvergencia();
		// BOt�o Iniciar algor�tmo.
		botaoIniciar = new JButton();
		botaoIniciar.setText("INICIAR ALGOR�TMO");
		painelLeft.add(botaoIniciar);
		comboTipoMutacao.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});
		botaoIniciar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				int numGeracoes = 0;
				int tamPopulacao = 0;
				double taxaCrosover = 0;
				int tipoMutacao = 0;
				try {
					logArea.setText(null);
					numGeracoes = Integer.valueOf(valoGeracoes.getText());
					taxaCrosover = Double.valueOf(valorCrossover.getText());
					tamPopulacao = Integer.valueOf(valoPopulacao.getText());
					String tipoMutacaoString = comboTipoMutacao
							.getSelectedItem().toString();
					switch (tipoMutacaoString) {
					case DETERMINISTICO:
						tipoMutacao = 1;
						break;
					case FEEDBACK_DA_BUSCA:
						tipoMutacao = 2;
						break;
					case AUTO_ADAPTATIVA:
						tipoMutacao = 3;
						break;
					default:
						tipoMutacao = 1;
						break;
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null,
							"Erro, insira parametros v�lidos");

				}
				tarefa.executaAlgoritmoGenetico(numGeracoes, tamPopulacao,
					 taxaCrosover, tipoMutacao);
			}
		});
		getContentPane().add(painelPrincipal);
		pack();
	}

	/**
	 * Monta o gr�fico
	 */
	public void montaGraficoConvergencia() {
		graficoConvergencia = new GraficoConvergencia(500, 500);
		JPanel img = graficoConvergencia.getImage();
		painelGrafico.removeAll();
		GroupLayout mainPanelLayout = new GroupLayout(painelGrafico);
		painelGrafico.setLayout(mainPanelLayout);
		GroupLayout.SequentialGroup hGroup = mainPanelLayout
				.createSequentialGroup();
		hGroup.addGap(5, 5, 5);
		hGroup.addComponent(img);
		mainPanelLayout.setHorizontalGroup(hGroup);
		GroupLayout.SequentialGroup vGroup = mainPanelLayout
				.createSequentialGroup();
		vGroup.addGap(5, 5, 5);
		vGroup.addComponent(img);
		mainPanelLayout.setVerticalGroup(vGroup);
		img.setVisible(true);
	}

	public void setLog(String texto) {
		logArea.append(texto + "\n");
		logArea.setCaretPosition(logArea.getText().length() - 1);
	}

	public GraficoConvergencia getGraficoConvergencia() {
		return graficoConvergencia;
	}

	public void setGraficoConvergencia(GraficoConvergencia graficoConvergencia) {
		this.graficoConvergencia = graficoConvergencia;
	}

	public JComboBox<String> getComboTipoMutacao() {
		return comboTipoMutacao;
	}

	public void setComboTipoMutacao(JComboBox<String> comboTipoMutacao) {
		this.comboTipoMutacao = comboTipoMutacao;
	}
}
