package ce;

import java.util.ArrayList;

/**
 * @author valdevir
 *
 */
public class Populacao {

	private ArrayList<Individuo> individuos;

	@SuppressWarnings("unused")
	private int tamPopulacao;

	/**
	 * Cria uma popula��o de indiv�duos aleat�rios.
	 * 
	 * @param tamPop
	 * @param numeroGenesByCromossomos
	 * @param isAleatorio
	 */
	public Populacao(int tamPop, boolean isAleatorio) {
		if (isAleatorio) {
			tamPopulacao = tamPop;
			individuos = geraIndividuos(tamPop);
		} else {
			individuos = new ArrayList<Individuo>();
		}
	}

	public Populacao() {
		individuos = new ArrayList<Individuo>();
	}

	/**
	 * Gera Indiv�duos aleatoriamente.
	 * 
	 * @param tamanhoPop
	 * @return
	 */
	private ArrayList<Individuo> geraIndividuos(int tamanhoPop) {
		ArrayList<Individuo> individuloList = new ArrayList<Individuo>();
		for (int i = 0; i < tamanhoPop; i++) {
			Individuo individuo = new Individuo(true);
			individuloList.add(individuo);
		}
		return individuloList;

	}

	/**
	 * Faz a m�dia de aptid�o da popula��o.
	 * 
	 * @return
	 */
	public double getMediaAptidao() {
		double media = getSomaDaAptidao() / getNumIndividuos();
		return media;
	}

	/**
	 * Obt�m Media Gene X1.
	 * 
	 * @return
	 */
	public double getMediaGeneX1() {
		double media = getSomaGeneX1() / getNumIndividuos();
		return media;
	}

	/**
	 * Obt�m Media Gene X2.
	 * 
	 * @return
	 */
	public double getMediaGeneX2() {
		double media = getSomaGeneX2() / getNumIndividuos();
		return media;
	}

	/**
	 * Soma a aptid�o da popula��o.
	 * 
	 * @return
	 */
	public double getSomaDaAptidao() {
		double total = 0;
		for (int i = 0; i < individuos.size(); i++) {
			if (individuos.get(i) != null) {
				total += individuos.get(i).getFitness();
			}
		}
		return total;
	}

	/**
	 * Soma Gene X1.
	 * 
	 * @return
	 */
	public double getSomaGeneX1() {
		double total = 0;
		for (int i = 0; i < individuos.size(); i++) {
			if (individuos.get(i) != null) {
				total += individuos.get(i).getCromossomo().geneX1Real;
			}
		}
		return total;
	}

	/**
	 * Soma Gene X2.
	 * 
	 * @return
	 */
	public double getSomaGeneX2() {
		double total = 0;
		for (int i = 0; i < individuos.size(); i++) {
			if (individuos.get(i) != null) {
				total += individuos.get(i).getCromossomo().geneX2Real;
			}
		}
		return total;

	}

	/**
	 * Soma total dos valores da fun��o.
	 * 
	 * @return
	 */
	public double getSomaValorFuncao() {
		double total = 0;
		for (int i = 0; i < individuos.size(); i++) {
			if (individuos.get(i) != null) {
				total += individuos.get(i).getValorFuncaoObjetivo();
			}
		}
		return total;
	}

	// N�mero de indiv�duos existente na popula��o.
	public int getNumIndividuos() {

		return individuos.size();
	}

	public int getTamPopulacao() {
		return individuos.size();
	}

	public void setTamPopulacao(int tamPopulacao) {
		this.tamPopulacao = tamPopulacao;
	}

	public ArrayList<Individuo> getIndividuos() {
		return individuos;
	}

	public void setIndividuos(ArrayList<Individuo> individuos) {
		this.individuos = individuos;
	}

}
