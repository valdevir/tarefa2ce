package ce;

import java.util.Locale;

public class Individuo implements Comparable<Individuo> {

	/**
	 * Corresponde ao conjunto fe genes (x1, x2) que ir� reprresentar o
	 * Cromossomo.
	 */
	private Cromossomo cromossomo;
	/**
	 * Valor de apdtid�o deste indiv�duo ou seja desta solu��o.
	 */
	private double fitness;

	/**
	 * Valor do indiv�duo na fun��o objetivo.
	 */
	private double valorFuncaoObjetivo;

	/**
	 * 
	 * 
	 * @param numGenes
	 */
	public Individuo(boolean isAleatorio) {
		cromossomo = new Cromossomo(isAleatorio);
	}

	public Individuo() {

	}

	public Individuo(Cromossomo cromossomo) {
		setCromossomo(cromossomo);
	}

	public Cromossomo getCromossomo() {
		return cromossomo;
	}

	public void setCromossomo(Cromossomo cromossomo) {
		this.cromossomo = cromossomo;
	}

	public double getFitness() {
		return fitness;
	}

	public void setFitness(double fitness) {
		this.fitness = Double
				.valueOf(String.format(Locale.US, "%.5f", fitness));
		
	}

	public double getValorFuncaoObjetivo() {
		return valorFuncaoObjetivo;
	}

	public void setValorFuncaoObjetivo(double valorFuncaoObjetico) {
		this.valorFuncaoObjetivo = Double
				.valueOf(String.format(Locale.US, "%.5f", valorFuncaoObjetico));
	}

	@Override
	public int compareTo(Individuo outroIndividuo) {
		if (this.valorFuncaoObjetivo < outroIndividuo.getValorFuncaoObjetivo()) {
			return -1;
		}
		if (this.valorFuncaoObjetivo > outroIndividuo.getValorFuncaoObjetivo()) {
			return 1;
		}
		return 0;
	}

}
