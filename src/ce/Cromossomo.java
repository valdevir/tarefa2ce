package ce;

import java.util.ArrayList;
import java.util.Random;

/**
 * @author valdevir
 *
 */
public class Cromossomo {

	double geneX1Real;
	double geneX2Real;

	/**
	 * @param geneX1Real
	 * @param geneX2Real
	 */
	public Cromossomo(double geneX1Real, double geneX2Real) {
		this.geneX1Real = geneX1Real;
		this.geneX2Real = geneX2Real;
	}

	/**
	 * @param isAleatorio
	 * @param tamanhoGene
	 */
	public Cromossomo(boolean isAleatorio) {
		if (isAleatorio) {
			createRandomCromossomo();
		}
	}


	/**
	 * 
	 */
	public void createRandomCromossomo() {
		Random random = new Random();
		double valorX1 = random.nextDouble() * 20;
		double valorX2 = random.nextDouble() * 20;
		if (valorX1 > 10.0) {
			valorX1 = (valorX1 - 10) * -1;
		}
		setGeneX1Real(valorX1);
		if (valorX2 > 10.0) {
			valorX2 = (valorX2 - 10) * -1;
		}
		setGeneX2Real(valorX2);

	}

	public double getGeneX1Real() {
		return geneX1Real;
	}

	public void setGeneX1Real(double geneX1Real) {
		this.geneX1Real = geneX1Real;
	}

	public double getGeneX2Real() {
		return geneX2Real;
	}

	public void setGeneX2Real(double geneX2Real) {
		this.geneX2Real = geneX2Real;
	}

}
