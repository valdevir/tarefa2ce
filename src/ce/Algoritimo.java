package ce;

import java.util.Collections;
import java.util.Random;

public class Algoritimo {

	// taxa de crossover de 70% - Conforme especificado exerc�cio.
	private double taxaDeCrossoverDefault = 0.7;
	// taxa de muta��o de 0,125% que � 1/8 (8-> n�mero de bits).
	private double taxaDeMutacaoDefault = 0.125;
	// Numero de Genes
	private int numGenes = 8;
	private int numMaxGeracoesDefault = 200;
	// tamanho da popula��o
	private int tamPopDefault = 100;
	private static final int maximo = 10;
	private static final int minimo = -10;
	// Este ser�o os dados que o algor�tmo trabalhar�.
	private double taxaDeCrossover;
	private double taxaDeMutacao;
	private int tamPop;
	private int numMaxGeracoes;

	public Algoritimo(int numGeracoes, int tamPopulacao, double taxaCrossover) {
		setNumMaxGeracoes(numGeracoes);
		setTamPop(tamPopulacao);
		setTaxaDeCrossover(taxaCrossover);
		// setTaxaDeMutacao(taxaMutacao);
	}

	public Algoritimo() {

	}

	/**
	 * Sele��o por roleta
	 * 
	 * @param populacao
	 */
	public Populacao selecaoRoleta(Populacao populacao) {
		double somaTotal = 0;
		Populacao populacaoAux = populacao;
		Populacao populacaIntermediaria = new Populacao(20, false);
		// Faz somta�rio de todos os fitness da popula��o.
		for (int i = 0; i < populacaoAux.getTamPopulacao(); i++) {
			somaTotal = somaTotal
					+ populacaoAux.getIndividuos().get(i).getFitness();
		}
		int count = 0;
		/*
		 * Arbitrariamente defini que a popula��o intermedi�ria poder� gerar
		 * filhos, a popula��o intermedi�ria ser� de tamanho 20.
		 */
		while (count < 20) {
			count++;
			Random r = new Random();
			double soma = 0;
			int tamanhoPop = populacaoAux.getTamPopulacao();
			for (int i = 0; i < tamanhoPop; i++) {
				// Gera valor randomico de 0 at� a somat�ria dos fitness.
				double valorSorteado = r.nextDouble() * somaTotal;
				/*
				 * Caso o valor encontrado maior que valor sorteado, escolhe
				 * indiv�duo.
				 */
				if (soma >= valorSorteado) {
					populacaIntermediaria.getIndividuos().add(
							populacaoAux.getIndividuos().get(i));
					populacao.getIndividuos().remove(i);
					break;
				} else {
					soma = soma
							+ populacaoAux.getIndividuos().get(i).getFitness();
				}
			}
		}

		return populacaIntermediaria;
	}

	/**
	 * Aplica��o Fun��o Alpine
	 * 
	 * @param populacao
	 */
	public void aplicaFuncaoAlpine(Populacao populacao) {
		for (int i = 0; i < populacao.getTamPopulacao(); i++) {
			Individuo individuo = populacao.getIndividuos().get(i);
			double soma = 0;
			double x1 = individuo.getCromossomo().getGeneX1Real();
			double x2 = individuo.getCromossomo().getGeneX2Real();
			double valorX1 = (double) Math.abs(x1 * (Math.sin(x1)) + 0.1 * x1);
			double valorX2 = (double) Math.abs(x1 * (Math.sin(x1)) + 0.1 * x2);
			soma = valorX1 + valorX2;
			individuo.setValorFuncaoObjetivo(soma);
		}
	}

	/**
	 * Aplica a fun��o de aptid�o proporcional.
	 * 
	 * @param populacao
	 */
	public void aplicaAptidaoProporcional(Populacao populacao) {
		// Realiza soma de todos os valores da fun��o da popula��o.
		double soma = populacao.getSomaValorFuncao();
		/*
		 * Percorre a popula��o e classifica dividindo o valor da fun��o do
		 * indiv�duo pela soma total.
		 */
		for (int i = 0; i < populacao.getTamPopulacao(); i++) {
			double valorFirness = (double) (populacao.getIndividuos().get(i)
					.getValorFuncaoObjetivo() / soma);
			populacao.getIndividuos().get(i).setFitness(valorFirness);
		}
		// ordena na �rdem crescnte.
		Collections.sort(populacao.getIndividuos());

	}

	/**
	 * Gera aptid�o por rankeamento linear. (Este m�todo n�o � utilizado).
	 * 
	 * @param populacao
	 */
	public void aplicaAtidaoRankingLinear(Populacao populacao) {
		// Orderna e, ordem crescente.
		Collections.sort(populacao.getIndividuos());
		for (int i = 0; i < populacao.getTamPopulacao(); i++) {
			Individuo individuo = populacao.getIndividuos().get(i);
			int min = getMinimo();
			int max = getMaximo();
			int tamanhoPop = populacao.getTamPopulacao();
			float divisao = ((float) (tamanhoPop - i))
					/ ((float) (tamanhoPop - 1));
			float fitness = min + (max - min) * divisao;
			individuo.setFitness(fitness);
		}

	}

	/**
	 * Realiza Crossover (aritm�tico) e muta��o, lembrando que ser� realizado
	 * crossover somente 20 indiv�duos da popula��o toal (100).
	 * 
	 * @param populacaoIntermediaria
	 * @return
	 */
	public Populacao realizaCrossoverMutacao(Populacao populacao) {
		// Populacao populacaoNova = new Populacao(20, false);
		Random r = new Random();
		for (int i = 0; i < populacao.getTamPopulacao(); i += 2) {
			float valorRandom = r.nextFloat();
			if (valorRandom < getTaxaDeCrossover()) {
				float alhpa = r.nextFloat();
				// Pai 1 X1
				double x1Pai1 = populacao.getIndividuos().get(i)
						.getCromossomo().getGeneX1Real();
				// Pai 2 X1
				double x2Pai1 = populacao.getIndividuos().get(i)
						.getCromossomo().getGeneX2Real();
				// Pai 1 X2
				double x1Pai2 = populacao.getIndividuos().get(i)
						.getCromossomo().getGeneX1Real();
				// Pai 2 X2
				double x2Pai2 = populacao.getIndividuos().get(i)
						.getCromossomo().getGeneX2Real();
				// Cria X1 de filho 1.
				double x1Filho1 = (alhpa * x1Pai1) + (1 - alhpa) * x1Pai2;
				// Cria X2 de filho 1.
				double x2Filho1 = (1 - alhpa) * x2Pai1 + x2Pai2;

				// Cria X1 filho 2
				double x1Filho2 = (alhpa * x1Pai2) * +(1 - alhpa) * x1Pai1;
				// Cria X2 filho 2
				double x2Filho2 = (alhpa * x2Pai2) * +(1 - alhpa) * x2Pai1;
				// Realiza muta��o e gera cromossomo filho 1
				// Cromossomo cromossomoFilho1 =
				// realizaMutacaoGaussiana(x1Filho1,
				// x2Filho1, populacao, i);
				Cromossomo cromossomoFilho1 = realizaMutacaoUniforme(x1Filho1,
						x2Filho1, populacao, i);
				// Realiza muta��o e gera cromossomo filho 2
				// Cromossomo cromossomoFilho2 =
				// realizaMutacaoGaussiana(x1Filho2,
				// x2Filho2, populacao, i);
				Cromossomo cromossomoFilho2 = realizaMutacaoUniforme(x1Filho2,
						x2Filho2, populacao, i);
				// Gera indiv�duo 1
				Individuo individuoFilho1 = new Individuo(cromossomoFilho1);
				// Gera indiv�duo 2
				Individuo individuoFilho2 = new Individuo(cromossomoFilho2);
				// Seta novo filho 1
				populacao.getIndividuos().set(i, individuoFilho1);
				// Seta novo filho 2
				populacao.getIndividuos().set(i + 1, individuoFilho2);

			}

		}
		return populacao;
	}

	private Cromossomo realizaMutacaoUniforme(double filho1x1, double filho1x2,
			Populacao populacao, int i) {
		Random r = new Random();
		int gene = r.nextInt(2);
		if (gene == 1) {
			filho1x1 = Utils.createRandomGene();
		} else {
			filho1x2 = Utils.createRandomGene();
		}
		Cromossomo cromossomo = new Cromossomo(filho1x1, filho1x2);
		return cromossomo;

	}

	/**
	 * Realiza Muta��o e gera novo Crmossomo filho
	 * 
	 * @param filho1x1
	 * @param filho1x2
	 * @return
	 */
	@SuppressWarnings("unused")
	private Cromossomo realizaMutacaoGaussiana(double filho1x1,
			double filho1x2, Populacao populacao, int i) {
		Random r = new Random();
		double geneX = 0;

		/*
		 * Gera aleatoriamnete qual o gene ser� alterado (s� ser� alterado um
		 * dos genes).
		 */
		int gene = r.nextInt(2);
		if (gene == 1) {
			// Calcula desvio padr�o patra gene X1.
			double desvioPadraoGeneX1 = Utils.calculaDesvioPadrao(populacao, 1);
			// Gera Distribui��o normal para gene X1
			// NormalDistributionImpl nDImpX1 = new NormalDistributionImpl(0,
			// desvioPadraoGeneX1);
			geneX = populacao.getIndividuos().get(i).getCromossomo()
					.getGeneX1Real();
			// Gera novo gene X1.
			// filho1x1 = filho1x1 + nDImpX1.cumulativeProbability(geneX);
			filho1x1 = filho1x1
					+ Utils.calculaDistribuicaoNormal(desvioPadraoGeneX1, 0,
							geneX);
		} else {
			// Calcula desvio padr�o patra gene X2.
			double desvioPadraoGeneX2 = Utils.calculaDesvioPadrao(populacao, 2);
			geneX = populacao.getIndividuos().get(i).getCromossomo()
					.getGeneX2Real();
			// Gera Distribui��o normal para gene X2
			// NormalDistributionImpl nDImpX2 = new
			// NormalDistributionImpl(0,
			// desvioPadraoGeneX2);
			// Gera novo gene X2.
			filho1x2 = filho1x1
					+ Utils.calculaDistribuicaoNormal(desvioPadraoGeneX2, 0,
							geneX);
		}

		// Gera cromossomo filho.
		Cromossomo cromossomo = new Cromossomo(filho1x1, filho1x2);
		return cromossomo;

	}

	public static int getMaximo() {
		return maximo;
	}

	public static int getMinimo() {
		return minimo;
	}

	public double getTaxaDeCrossoverDefault() {
		return taxaDeCrossoverDefault;
	}

	public void setTaxaDeCrossoverDefault(double taxaDeCrossoverDefault) {
		this.taxaDeCrossoverDefault = taxaDeCrossoverDefault;
	}

	public double getTaxaDeMutacaoDefault() {
		return taxaDeMutacaoDefault;
	}

	public void setTaxaDeMutacaoDefault(double taxaDeMutacaoDefault) {
		this.taxaDeMutacaoDefault = taxaDeMutacaoDefault;
	}

	public int getNumGenes() {
		return numGenes;
	}

	public void setNumGenes(int numGenes) {
		this.numGenes = numGenes;
	}

	public int getNumMaxGeracoesDefault() {
		return numMaxGeracoesDefault;
	}

	public void setNumMaxGeracoesDefault(int numMaxGeracoesDefault) {
		this.numMaxGeracoesDefault = numMaxGeracoesDefault;
	}

	public int getTamPopDefault() {
		return tamPopDefault;
	}

	public void setTamPopDefault(int tamPopDefault) {
		this.tamPopDefault = tamPopDefault;
	}

	public double getTaxaDeCrossover() {
		return taxaDeCrossover;
	}

	public void setTaxaDeCrossover(double taxaDeCrossover) {
		this.taxaDeCrossover = taxaDeCrossover;
	}

	public double getTaxaDeMutacao() {
		return taxaDeMutacao;
	}

	public void setTaxaDeMutacao(double taxaDeMutacao) {
		this.taxaDeMutacao = taxaDeMutacao;
	}

	public int getTamPop() {
		return tamPop;
	}

	public void setTamPop(int tamPop) {
		this.tamPop = tamPop;
	}

	public int getNumMaxGeracoes() {
		return numMaxGeracoes;
	}

	public void setNumMaxGeracoes(int numMaxGeracoes) {
		this.numMaxGeracoes = numMaxGeracoes;
	}

}
