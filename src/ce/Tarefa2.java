package ce;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Tarefa2 {
	public static Interface telaInterface;

	public static void main(String[] args) {
		// Cria a tela
		telaInterface = new Interface();
		telaInterface.setVisible(true);
		System.out.println("Teste");
	}

	public void executaAlgoritmoGenetico(int numGeracoes, int tamPopulacao,
			double taxaCrossover, int tipoMutacao) {
		try {

			Algoritimo algoritimo = new Algoritimo(numGeracoes, tamPopulacao,
					taxaCrossover);
			// Cria arquivo de log do algor�tmo.
			File file = new File("logGA.txt");
			OutputStream output = new FileOutputStream(file);
			// 1 passo � gerar a popula��o inicial.
			Populacao populacao = new Populacao(algoritimo.getTamPop(), true);
			StringBuilder concat = new StringBuilder("Individuos: \n");
			concat.append("Tamanho poupla��o = " + populacao.getTamPopulacao());
			concat.append("Gera��o " + 0 + ": \n");
			// Aplica fun��o Alpine
			algoritimo.aplicaFuncaoAlpine(populacao);
			// Aplica Fun��o de aptid�o e gera a aptid�o
			algoritimo.aplicaAptidaoProporcional(populacao);
			concat = geraLog(populacao, 0);
			// Gera log na tela.
			telaInterface.setLog(geraLogAreaRodada(populacao, 0));
			int count = 0;
			while (count < concat.toString().length()) {
				output.write(concat.charAt(count));
				count++;
			}
			int geracao = 0;
			// loop at� crit�rio de parada
			while (geracao < algoritimo.getNumMaxGeracoes()) {
				geracao++;
				// Aplica Crossover e muta��o.
				algoritimo.realizaCrossoverMutacao(populacao);
				algoritimo.aplicaFuncaoAlpine(populacao);
				algoritimo.aplicaAptidaoProporcional(populacao);
				StringBuilder sb = geraLog(populacao, geracao);
				telaInterface.setLog(geraLogAreaRodada(populacao, geracao));
				// GraficoConvergencia
				telaInterface.getGraficoConvergencia().update(
						geracao,
						populacao,
						populacao.getIndividuos().get(0),
						populacao.getIndividuos().get(
								populacao.getTamPopulacao() - 1), 0);

				count = 0;
				while (count < sb.toString().length()) {
					output.write(sb.charAt(count));
					count++;
				}

			}
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String geraLogAreaRodada(Populacao populacao, int geracao) {
		StringBuilder textoLog = new StringBuilder();
		textoLog.append("Gera��o: " + geracao + "\n");
		textoLog.append("Aptid�o Melhor Indiv�duo: "
				+ populacao.getIndividuos()
						.get(populacao.getTamPopulacao() - 1).getFitness()
				+ "\n");
		textoLog.append("Aptid�o Pior Indiv�duo : "
				+ populacao.getIndividuos().get(0).getFitness() + "\n");
		textoLog.append("Aptid�o M�dia: " + populacao.getMediaAptidao());
		textoLog.append("\n\n");

		return textoLog.toString();

	}

	/**
	 * Gera Arquivo de Log.
	 * 
	 * @param populacao
	 * @param geracao
	 * @return
	 */
	private static StringBuilder geraLog(Populacao populacao, int geracao) {
		StringBuilder concat = new StringBuilder();
		concat.append("\n Individuos Geracao " + geracao + "\n");
		concat.append("N -  Vl.X1Real, VlX2Real,  VlFObj   , Fitness\n");
		for (int i = 0; i < populacao.getNumIndividuos(); i++) {
			concat.append(i + " - ");

			concat.append(
					populacao.getIndividuos().get(i).getCromossomo()
							.getGeneX1Real()).append(", ");
			concat.append(
					populacao.getIndividuos().get(i).getCromossomo()
							.getGeneX2Real()).append(", ");
			concat.append(
					populacao.getIndividuos().get(i).getValorFuncaoObjetivo())
					.append(", ");
			concat.append(populacao.getIndividuos().get(i).getFitness());
			concat.append("\n");
		}
		return concat;
	}

}
