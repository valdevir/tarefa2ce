package ce;

import java.util.Random;

public class Utils {

	public static double calculaDesvioPadrao(Populacao populacao, int gene) {
		double desvioPadrao = 0;
		double somaMediaQuadrado = 0;
		double geneX = 0;
		double mediaGeneX = 0;
		double valorVariancia = 0;
		if (gene == 1) {
			for (int i = 0; i < populacao.getTamPopulacao(); i++) {
				geneX = populacao.getIndividuos().get(i).getCromossomo()
						.getGeneX1Real();
				mediaGeneX = populacao.getMediaGeneX1();
				valorVariancia = Math.pow((geneX - mediaGeneX), 2);
				somaMediaQuadrado = somaMediaQuadrado + valorVariancia;
			}
		} else {
			for (int i = 0; i < populacao.getTamPopulacao(); i++) {
				geneX = populacao.getIndividuos().get(i).getCromossomo()
						.getGeneX2Real();
				mediaGeneX = populacao.getMediaGeneX2();
				valorVariancia = Math.pow((geneX - mediaGeneX), 2);
				somaMediaQuadrado = somaMediaQuadrado + valorVariancia;
			}
		}
		desvioPadrao = somaMediaQuadrado / populacao.getNumIndividuos();

		return desvioPadrao;
	}

	public static double calculaDistribuicaoNormal(double desvioPadrao,
			double media, double x) {
		double z = 0;
		z = (x - media) / desvioPadrao;
		return z;
	}

	/**
	 * Cria um gene randomicamente.
	 * 
	 * @return
	 */
	public static double createRandomGene() {
		Random random = new Random();
		double valorX = random.nextDouble() * 20;
		if (valorX > 10.0) {
			valorX = (valorX - 10) * -1;
		}
		return valorX;

	}
}
